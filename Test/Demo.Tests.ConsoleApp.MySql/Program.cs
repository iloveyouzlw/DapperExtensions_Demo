﻿using HY.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions.Lambda;
using DapperExtensions.Demo.Model.Entities;
using DapperExtensions;
using HY.DataAccess;

namespace Demo.Tests.ConsoleApp.MySql
{
    class Program
    {
        static void Main(string[] args)
        {





            LambdaQueryHelper<UsersEntity> LambdaObj = DapperExtension.Instance(DataBaseType.MySql).LambdaQuery<UsersEntity>((IDbConnection)Common.GetConnByKey(), null, null);


            var from = LambdaObj.Where(p => p.UserId == 1).Top(1);
            IEnumerable<UsersEntity> list = from.ToList();
            Console.WriteLine("测试结束");
            Console.ReadLine();
        }
    }
}
